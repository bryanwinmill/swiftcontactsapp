//
//  SingleContactViewController.swift
//  SwiftContactsApp
//
//  Created by Bryan Winmill on 10/13/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class SingleContactViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate {
    
    let realm = try! Realm()
    var incomingContact : Contacts? = nil
    
    @IBOutlet var name: UILabel!
    @IBOutlet var phone: UILabel!
    @IBOutlet var address: UILabel!
    @IBOutlet var email: UILabel!
    
    @IBAction func callNumber(_ sender: UIButton) {
        
        if let change = incomingContact {
            if let url = URL(string: "tel://\(change.phoneNumber)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @IBAction func textNumber(_ sender: UIButton) {
        
        if let change = incomingContact {
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = "Message Body"
                controller.recipients = [change.phoneNumber]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func sendEmail(_ sender: UIButton) {
        
        if let change = incomingContact {
            let composeEmail = MFMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                composeEmail.mailComposeDelegate = self
                composeEmail.setToRecipients([change.email])
                composeEmail.setSubject("Email")
                composeEmail.setMessageBody("This is what I want to send.", isHTML: false)
                self.present(composeEmail, animated: true, completion: nil)
            }
        }
    }    
    
    @IBAction func editContact(_ sender: UIBarButtonItem) {
        //self.performSegue(withIdentifier: "editContact", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let change = incomingContact {
            
            name.text = change.firstName + " " + change.lastName
            phone.text = "Phone Number: " + change.phoneNumber
            address.text = "Address: " + change.address
            email.text = "Email: " + change.email
        }
        
        
 
 
        let editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(buttonTapped(_:)))
        
        self.navigationItem.rightBarButtonItems = [editButton]
 
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let change = incomingContact {
            name.text = change.firstName + " " + change.lastName
            phone.text = "Phone Number: " + change.phoneNumber
            address.text = "Address: " + change.address
            email.text = "Email: " + change.email
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func buttonTapped (_ sender : UIButton) {
        
        self.performSegue(withIdentifier: "editContact", sender: nil)
        
        //if let change = incomingContact {
            
            /*try! realm.write() {
                change.firstName = firstName.text!
                change.lastName = lastName.text!
                change.phoneNumber = phone.text!
                change.address = address.text!
                change.email = email.text!
            }
        }
        else {
            
            let newContact = Contacts()
            newContact.firstName = firstName.text!
            newContact.lastName = lastName.text!
            newContact.phoneNumber = phone.text!
            newContact.address = address.text!
            newContact.email = email.text!
            
            try! realm.write() {
                self.realm.add(newContact)
            }
        }*/
        
        
        
        //self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editContact" {
            let destination = segue.destination as! AddViewController
            let contact = incomingContact//contacts![(contactsTableView.indexPathForSelectedRow?.row)!]
            destination.incomingContact = contact
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

