//
//  ViewController.swift
//  SwiftContactsApp
//
//  Created by Bryan Winmill on 10/12/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import UIKit
import RealmSwift

class AddViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var phone: UITextField!
    @IBOutlet var address: UITextField!
    @IBOutlet var email: UITextField!
    
    let realm = try! Realm()
    var incomingContact : Contacts? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let change = incomingContact {
            firstName.text = change.firstName
            lastName.text = change.lastName
            phone.text = change.phoneNumber
            address.text = change.address
            email.text = change.email
        }
        
        self.firstName.delegate = self
        self.lastName.delegate = self
        self.phone.delegate = self
        self.address.delegate = self
        self.email.delegate = self
        
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(buttonTapped(_:)))
        
        self.navigationItem.rightBarButtonItems = [saveButton]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    
    @objc func buttonTapped (_ sender : UIButton) {
        
        if let change = incomingContact {
            
            try! realm.write() {
                change.firstName = firstName.text!
                change.lastName = lastName.text!
                change.phoneNumber = phone.text!
                change.address = address.text!
                change.email = email.text!
            }
        }
        else {
            
            let newContact = Contacts()
            newContact.firstName = firstName.text!
            newContact.lastName = lastName.text!
            newContact.phoneNumber = phone.text!
            newContact.address = address.text!
            newContact.email = email.text!
            
            try! realm.write() {
                self.realm.add(newContact)
            }
        }
                
        self.navigationController?.popViewController(animated: true)
    }


}

