//
//  Contacts.swift
//  SwiftContactsApp
//
//  Created by Bryan Winmill on 10/12/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import RealmSwift

class Contacts : Object {
    dynamic var firstName = ""
    dynamic var lastName = ""
    dynamic var phoneNumber = ""
    dynamic var address = ""
    dynamic var email = ""
}
