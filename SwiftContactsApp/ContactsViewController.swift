//
//  ContactsTableViewController.swift
//  SwiftContactsApp
//
//  Created by Bryan Winmill on 10/12/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import UIKit
import RealmSwift

var contacts : Results<Contacts>?
let realm = try! Realm()

class ContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var contactsTableView: UITableView!
    
    
    
    @IBAction func addContact(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "addToContactsView", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contacts = realm.objects(Contacts.self)
        contactsTableView.dataSource = self
        contactsTableView.delegate = self
        update()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        update()
    }
    
    func update() {
        contactsTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return contacts!.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let contact = contacts![indexPath.row]
        cell.textLabel?.text = contact.firstName + " " + contact.lastName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            try! realm.write {
                realm.delete(contacts![indexPath.row])
            }
            update()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewContact" {
            let destination = segue.destination as! SingleContactViewController
            let contact = contacts![(contactsTableView.indexPathForSelectedRow?.row)!]
            destination.incomingContact = contact
        }
    }

}
